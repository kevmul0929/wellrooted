<?php

namespace Tests\Feature;

use App\Testimonial;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ViewingTestimonialsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_view_a_published_testimonial()
    {
        // $this->withoutExceptionHandling();
        $testimonial = factory(Testimonial::class)->states('published')->create([
            'name' => 'Kevin',
            'title' => 'My First Testimonial',
            'email' => 'kmulholland123@gmail.com',
            'body' => 'Frankie was a pleasure to work with!',
            'approved' => 1
        ]);

        $response = $this->get('/testimonials/');
        $response->assertStatus(200);

        $response->assertSee('Kevin');
        $response->assertSee('My First Testimonial');
        $response->assertSee('Frankie was a pleasure to work with!');
    }


    /** @test */
    public function a_user_cannot_view_an_unpublished_testimonial()
    {
        // $this->withoutExceptionHandling();
        $testimonial = factory(Testimonial::class)->create([
            'name' => 'Kevin',
            'email' => 'kmulholland123@gmail.com',
            'body' => 'Frankie was a pleasure to work with!',
            'approved' => 0
        ]);

        $response = $this->get('/testimonial/' . $testimonial->id);
        $response->assertStatus(404);
    }


    /** @test */
    public function a_user_can_view_all_published_testimonials()
    {
        $publishedTestimonialA = factory(Testimonial::class)->states('published')->create(['name' => 'PublishedA']);
        $publishedTestimonialB = factory(Testimonial::class)->states('published')->create(['name' => 'PublishedB']);
        $unpublishedTestimonial = factory(Testimonial::class)->states('unpublished')->create(['name' => 'Unpublished']);

        $response = $this->get('/testimonials');

        $response->assertSee('PublishedA');
        $response->assertSee('PublishedB');
        $response->assertDontSee('Unpublished');
    }

}
