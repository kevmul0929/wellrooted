<?php

namespace Tests\Feature;

use App\Testimonial;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PublishingTestimonialsTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function an_admin_can_publish_a_submitted_testimonial()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $newTestimonial = factory(Testimonial::class)->create([
            'name' => 'Kevin'
        ]);

        $this->patch('admin/testimonial/'.$newTestimonial->id, [
            'published_at' => Carbon::parse('-1 day')
        ]);

        $response = $this->get('/testimonial/' . $newTestimonial->id);
        $response->assertSee('Kevin');
    }


    /** @test */
    public function an_admin_can_update_a_submitted_testimonial()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $newTestimonial = factory(Testimonial::class)->create([
            'name' => 'Kevin'
        ]);

        $this->patch('admin/testimonial/'.$newTestimonial->id, [
            'name' => 'Carl',
            'published' => true
        ]);

        $response = $this->get('/testimonial/' . $newTestimonial->id);
        $response->assertSee('Carl');
    }



}
