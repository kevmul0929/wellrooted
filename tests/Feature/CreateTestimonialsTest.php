<?php

namespace Tests\Feature;

use App\Testimonial;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CreateTestimonialsTest extends TestCase
{

    use RefreshDatabase;

    /**
     * Assert that the test has errors
     *
     * @param  String $field
     */
    private function assertValidationError($response, $field)
    {
        $response->assertStatus(422);
        $response->assertJsonValidationErrors($field);
    }

    /*|--------------------------------------------------------
      | TESTS
      |------------------------------------------------------*/

    /** @test */
    public function a_user_can_submit_a_review()
    {
        $this->withoutExceptionHandling();
        $testimonial = factory(Testimonial::class)->make([
            'name' => 'Kevin Mulholland',
            'title' => 'My first Testimonial',
            'email' => 'kmulholland123@gmail.com',
            'body' => 'Frankie was a pleasure to work with!',
        ]);

        $response = $this->json('POST', '/testimonial', $testimonial->toArray());
        $response->assertStatus(302);

        $this->assertDatabaseHas('testimonials', [
            'name' => 'Kevin Mulholland',
            'title' => 'My first Testimonial',
            'email' => 'kmulholland123@gmail.com',
            'body' => 'Frankie was a pleasure to work with!',
        ]);
    }


    /** @test */
    public function a_user_can_upload_an_image()
    {
        $this->withoutExceptionHandling();
        Storage::fake('public');
        $file = File::image('profile_image.png');

        $testimonial = factory(Testimonial::class)->states('published')->make([
            'profile_image' => $file
        ]);

        $response = $this->json('POST', '/testimonial', $testimonial->toArray());

        tap(Testimonial::first(), function ($testimonial) use ($file) {
            $this->assertNotNull($testimonial->profile_image);
            $this->assertFileExists(public_path() . $testimonial->profile_image);
        });
    }


    /** @test */
    public function profile_image_must_be_an_image()
    {
        Storage::fake('public');
        $file = File::create('not_a_profile_image.pdf');

        $testimonial = factory(Testimonial::class)->states('published')->make([
            'profile_image' => $file
        ]);

        $response = $this->json('POST', '/testimonial', $testimonial->toArray());

        $this->assertValidationError($response, 'profile_image');
    }


    /** @test */
    public function a_testimonial_can_have_an_image_removed()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        Storage::fake('public');
        $file = File::image('profile_image.png');
        $testimonial = factory(Testimonial::class)->states('published')->make([
            'profile_image' => $file
        ]);
        $response = $this->json('POST', "/testimonial", $testimonial->toArray());
        $response->assertStatus(302);

        $testimonial = Testimonial::first();
        $testimonial->profile_image = null;
        $response = $this->actingAs($user)->json('PATCH', "/admin/testimonial/{$testimonial->id}", $testimonial->toArray());

        $this->assertEquals(null, $testimonial->fresh()->profile_image);
        $this->assertDatabaseHas('testimonials', [
            'profile_image' => null,
        ]);
    }


    /** @test */
    public function a_name_is_required_to_create_a_testimonial()
    {
        $testimonial = factory(Testimonial::class)->make([
            'name' => '',
        ]);

        $response = $this->json('POST', '/testimonial', $testimonial->toArray());
        $this->assertValidationError($response, 'name');
    }


    /** @test */
    public function a_title_is_required_to_create_a_testimonial()
    {
        $testimonial = factory(Testimonial::class)->make([
            'title' => '',
        ]);

        $response = $this->json('POST', '/testimonial', $testimonial->toArray());
        $this->assertValidationError($response, 'title');

    }


    /** @test */
    public function an_email_is_required_to_submit_a_review()
    {
        $testimonial = factory(Testimonial::class)->make([
            'email' => '',
        ]);

        $response = $this->json('POST', '/testimonial', $testimonial->toArray());
        $this->assertValidationError($response, 'email');
    }


    /** @test */
    public function an_email_must_be_a_valid_email()
    {
        $testimonial = factory(Testimonial::class)->make([
            'email' => 'INVALID EMAIL',
        ]);

        $response = $this->json('POST', '/testimonial', $testimonial->toArray());
        $this->assertValidationError($response, 'email');
    }


    /** @test */
    public function a_written_testimonial_is_required_to_submit_a_testimonial()
    {
        $testimonial = factory(Testimonial::class)->make([
            'body' => '',
        ]);

        $response = $this->json('POST', '/testimonial', $testimonial->toArray());
        $this->assertValidationError($response, 'body');
    }

    /** @test */
    public function a_testimonial_with_image_can_be_updated_without_loosing_current_image()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        Storage::fake('public');
        $file = File::image('profile_image.png');
        $testimonial = factory(Testimonial::class)->states('published')->make([
            'name' => 'Wrong Name',
            'profile_image' => $file
        ]);
        $response = $this->json('POST', "/testimonial", $testimonial->toArray());
        $response->assertStatus(302);

        $testimonial = Testimonial::first()->fresh();
        $this->assertEquals('Wrong Name', $testimonial->name);
        $this->assertNotNull($testimonial->profile_image);
        $updatedFields = [
            'name' => 'Correct Name',
        ];
        $response = $this->actingAs($user)->json('PATCH', "/admin/testimonial/{$testimonial->id}", array_merge($testimonial->toArray(), $updatedFields));

        tap(Testimonial::first(), function ($testimonial) use ($response) {
            $this->assertEquals('Correct Name', $testimonial->name);
            $this->assertNotNull($testimonial->profile_image);

        });

    }

}
