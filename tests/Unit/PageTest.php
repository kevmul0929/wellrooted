<?php

use App\Pages\MainPages;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class PageTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function a_page_can_be_ordered_by_a_number()
    {
        $page = factory(MainPages::class)->states('with_slug')->create([
        	'order_by' => 1,
        ]);

        $this->assertEquals(1, $page->order_by);
    }

    /** @test */
    public function a_page_can_be_posted_to_site()
    {
        $this->withoutExceptionHandling();
        $this->actingAs(factory(User::class)->create());

        $page = [
            'title' => 'My first page',
            'body' => 'the body of my page',
            'excerpt' => 'Fake excerpt',
            'image' => UploadedFile::fake()->image('avatar.jpg'),
            'button_text' => 'Read More',
            'order_by' => 1
        ];

        $response = $this->post('admin/pages', $page);

        $all = MainPages::all();

        $this->assertDatabaseHas('main_pages', ['title' => 'My first page']);
        $this->assertEquals(1, count($all));
    }

    /** @test */
    public function a_page_can_reorder_older_pages()
    {
        $this->withoutMiddleware();

        $page1 = MainPages::persist(factory(MainPages::class)->make(['order_by' => 3]), '');
        $page2 = MainPages::persist(factory(MainPages::class)->make(['order_by' => 4]), '');
        $page3 = MainPages::persist(factory(MainPages::class)->make(['order_by' => 3]), '');

        $this->assertEquals(4, $page1->fresh()->order_by);
        $this->assertEquals(5, $page2->fresh()->order_by);
        $this->assertEquals(3, $page3->fresh()->order_by);
    }

    /** @test */
    public function a_page_has_a_slug_given_to_it()
    {
        $page = factory(MainPages::class)->make(['title' => 'My Best Page']);

        $page = MainPages::persist($page, '');

        $this->assertEquals('my-best-page', $page->slug);
    }
}
