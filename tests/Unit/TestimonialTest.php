<?php

namespace Tests\Unit;

use App\Testimonial;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class TestimonialTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_testimonial_can_be_published()
    {
        $testimonial = factory(Testimonial::class)->create();

        $testimonial->publish();

        $this->assertTrue($testimonial->approved);
    }

    /** @test */
    public function a_testimonial_can_have_an_image_removed()
    {
        $this->withoutExceptionHandling();
        $testimonial = factory(Testimonial::class)->make([
            'image' => UploadedFile::fake()->image('firstImage.jpg')
        ]);
        $response = $this->actingAs(factory(User::class)->make())->post('/testimonial', $testimonial->toArray());
        $response->assertStatus(302);

        $this->assertNotNull($testimonial->fresh()->image);

    }

    /** @test */
    public function a_testimonial_can_make_an_excpert()
    {
        $testimonial = factory(Testimonial::class)->make([
            'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit, tellus scelerisque sagittis mollis, felis diam consectetur orci, quis cursus risus nunc id felis. Morbi quis risus pretium, fermentum ex ac, porttitor quam. Mauris leo sem, pretium vitae lacinia vitae, tristique non orci. Maecenas non efficitur felis, posuere ullamcorper quam. Maecenas mattis ex sit amet metus molestie placerat. Integer nulla diam, egestas sit amet aliquam et, tristique ac nunc. Fusce hendrerit pulvinar erat eget dignissim. Donec arcu metus, interdum nec leo nec, malesuada egestas leo. Ut sollicitudin leo id turpis lacinia ultricies. Mauris diam justo, facilisis non neque et, aliquam mattis arcu. Cras accumsan dui a lectus blandit ultricies. Phasellus vehicula ultrices lorem eget lacinia. Sed lacus urna, pulvinar quis sodales convallis, imperdiet eu justo. Duis luctus non ex et faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque quis suscipit lacus.'
        ]);

        $this->assertEquals('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit, tellus scelerisque sagittis mollis, felis diam consectetur orci, quis cursus risus nunc id felis. Morbi quis risus pretium, fermentum ex ac, porttitor quam. Mauris leo sem, pretium vitae lacinia vitae, tristique non orci. Maecenas non efficitur felis, posuere ullamcorper quam. ...<a style="bottom: 0;right: 0;" class="Button Button--Primary pos-a" href="/testimonial/">Read More</a>', $testimonial->excerpt);
    }
}
