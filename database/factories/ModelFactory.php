<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Blog::class, function (Faker\Generator $faker) {
    return [
		'title' => $faker->sentence(4),
		'body' => $faker->paragraph,
        'image' => $faker->imageUrl(rand(400,1200), rand(200,1000),'cats',true,'Faker'),
	];
});


$factory->define(App\Pages\Pages::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(4),
        'body' => $faker->paragraphs(rand(5, 20), true),
        'slug' => str_replace(' ', '-', $faker->sentence(4))
    ];
});

$factory->define(App\Pages\MainPages::class, function (Faker\Generator $faker) {
    return [
        'title' => 'Example Title',
        'excerpt' => 'Example Excerpt',
        'body' => $faker->paragraphs(rand(5, 20), true),
        'image' => 'storage/img/2017/mar/2017-03-14_19-27-16__Screenshot_(13).png',
        'icon' => 'storage/img/2017/mar/2017-03-14_19-27-16__Screenshot_(13).png',
        'order_by' => rand(0,20),
    ];
});


$factory->state(App\Pages\MainPages::class, 'with_slug', function ($faker) {
    return [
        'slug' => str_replace(' ', '-', $faker->sentence(4)),
    ];
});
