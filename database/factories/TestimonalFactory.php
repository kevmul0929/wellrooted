<?php

use App\Testimonial;
use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;

$factory->define(Testimonial::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'title' => $faker->words(3,6),
        'email' => $faker->email,
        'body' => $faker->paragraphs(1,5),
        'image' => UploadedFile::fake()->image('fakeImage.jpg')
    ];
});

$factory->state(Testimonial::class, 'published', function ($faker) {
    return [
        'approved' => 1
    ];
});

$factory->state(Testimonial::class, 'unpublished', function ($faker) {
    return [
        'approved' => 0
    ];
});
