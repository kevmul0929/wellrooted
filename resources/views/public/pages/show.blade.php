@extends('public.layout.master')

@section('content')


@if($page->image)
<div class="Hero is-page">
    <img src="{{$page->size('wide')}}" alt="">
    <div class="Container pos-a">
        <div class="Hero__body Content text-is-centered">
            <h1 class="text-is-white">{{ $page->title }}</h1>
        </div>
    </div>
</div>
<section class="Container m-t-100 p-b-100">
@endif

{!! $page->body !!}
</section>

@endsection
