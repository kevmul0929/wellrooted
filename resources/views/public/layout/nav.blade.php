<nav class="Navigation Nav--Fixed">
	<section class="Container">
        <div class="Navbar">

            <div class="Navbar__brand">
                <div class="Logo p-10 is-white">
                    <a href="/">
                        <?php echo file_get_contents(public_path() . '/img/FGH__Logo__V3.svg');?>
                    </a>
                </div>
                <div class="Hamburger" @click="toggleNav" :class="{'is-active': displayNav}">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="Navbar__menu" :class="{'is-active': displayNav}">
                <div class="Navbar__menu--Start">
                </div>
                <div class="Navbar__menu--End">
                    <ul class="Nav__list">
                        @include('public.layout.nav-list')
                    </ul>
                </div>
            </div>

        </div>{{-- Navbar --}}
    </section>
</nav>
