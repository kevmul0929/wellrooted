<footer class="Footer p-t-40">

<div class="Container">
    <div class="Columns">
        <div class="Column text-is-centered">
            <h5>Legal Medical Disclaimer</h5>
            <p>
                Information and statements made on the this website, and all our associated literature are for educational purposes only and are not intended to diagnose, treat, cure or prevent any disease. If you have a medical condition, we recommend that you consult your physician of choice. Any information given is for health practitioners, scientists, researchers and individuals to ascertain its accuracy, efficacy and usefulness in medical, health and wellness conditions, and may not have been assessed by the US FDA.
            </p>
        </div>
    </div><!-- Columns -->
</div><!-- Container -->
<div class="Background--Primary_Dark">
    <div class="Container text-is-centered">
        <p>
            Frankie Giustino &copy;{{ date('Y')}} all rights reserved
        </p>
    </div>
</div>

</footer>
