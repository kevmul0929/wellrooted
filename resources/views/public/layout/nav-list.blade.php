@inject('pages', 'App\Pages\MainPages')


<li class="Nav__item">
	<a href="/blog">Blog</a>
</li>

<li class="Nav__item">
    <a href="/testimonials">Testimonials</a>
    <ul class="Nav__list--Dropdown">
        <li class="Nav__item">
            <a href="/testimonial/create">Leave a Testimonial</a>
        </li>
    </ul>
</li>


@if($pages)
<li class="Nav__item">
	<span>Info</span>
	<ul class="Nav__list--Dropdown">
		@foreach($pages->where('approved', '=', 1)->get() as $page)
			<li class="Nav__item">
				<a href="/pages/{{$page->slug}}">
					{{ $page->title }}
				</a>
			</li>
		@endforeach
	</ul>
</li>
@endif

<li class="Nav__item">
	<a href="/contact">Contact</a>
</li>
@if(Auth::user())
<li class="Nav__item">
	<a href="/admin/blog/">Admin</a>
</li>
@endif
