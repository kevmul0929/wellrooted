<!DOCTYPE html>
<html>
<head>
    <title>Frankie G Health</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{mix('/css/app.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scaleable=yes">
    @yield('head')
</head>
<body>
    <div class="Wrapper" id="app" style="flex:1">
        @include('public.layout.nav')
        @yield('content')
        <flash
            title="{{ session('flash_message')['title'] }}"
            message="{{ session('flash_message')['body'] }}"
            type="{{ session('flash_message')['level'] }}"
            important="{{ session('flash_message')['important'] }}"
        ></flash>
    </div><!-- #app -->
    @include('public.layout.footer')
<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('/js/app.js') }}"></script>
@yield('footer.scripts')
</body>
</html>
