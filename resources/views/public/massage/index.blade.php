@extends('public.layout.master')

@section('content')
<section class="Container is-page">
<h1 class="Title">{{$page->title}}</h1>
<div class="Body">
	<p>{{$page->body}}</p>
</div>
</section>
@endsection