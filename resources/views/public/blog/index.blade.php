@extends('public.layout.master')

@section('content')
<section class="Container p-t-100">
@if($blogs->count() > 0)
	@foreach($blogs as $artical)
		@include('public.blog.artical')
	@endforeach
@else
	<h2>Sorry!</h2>
	<p>No posts yet. Please check back soon!</p>
@endif
</section>

@stop
