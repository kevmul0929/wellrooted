@extends('public.layout.master')

@section('content')


@if($artical->image)
<div class="Hero m-t-75">
    <img src="{{$artical->size('wide')}}" alt="">
    <div class="Container pos-a">
        <div class="Hero__body Content text-is-centered">
            <h1 class="text-is-white">{{ $artical->title }}</h1>
        </div>
    </div>
</div>
<section class="Container m-t-100">
@endif

{!! $artical->body !!}
</section>

@endsection
