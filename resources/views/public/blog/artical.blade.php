<artical class="Card m-t-75 m-b-75">
    @if($artical->image)
        <div class="Hero">
            <img src="{{ $artical->size('medium') }}" alt="{{ $artical->title }}">
        </div>
    @endif
    <div class="Card__body">
        <h1>{{ $artical->title }}</h1>
        <p>{{ $artical->excerpt }}</p>
        <a class="Button Button--Primary" href="/blog/{{$artical->slug}}">Read more...</a>
    </div>
</artical>
