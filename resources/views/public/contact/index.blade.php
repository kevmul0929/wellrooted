@extends('public.layout.master')

@section('content')

<div class="Container is-page p-t-20">
	<h1>Contact me</h1>
    <hr>
	@include('errors.list')
	<div class="Grid is-spaced">
		<div class="Column is-12-wide is-6-wide-sm m-y-20">
			<form class="Form flex-column" action="/contact" method="POST">
				{{csrf_field()}}
				<div class="Form__group p-t-0">
					<label for="name" class="Form__label">Name</label>
					<input type="text" id="name" name="name" class="Form__input">
				</div>
				<div class="Form__group">
					<label for="email" class="Form__label">E-mail</label>
					<input type="text" id="email" name="email" class="Form__input">
				</div>
				<div class="Form__group">
					<label for="body" class="Form__label">Your Message</label>
					<textarea name="body" class="Form__input Form__textarea" cols="30" rows="10" placeholder="Your Message"></textarea>
				</div>
				<div class="Form__group flex-row align-items-center">
					<label class="Form__switch m-r-10">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="Form__slider"></span>
                    </label>
					<span>Send yourself a copy?</span>
				</div>
				<div class="">
					<button type="submit" id="submit" name="submit" class="Button Button--Primary">Submit</button>
				</div>
			</form>
		</div><!-- Grid is-half -->
		<div class="Column is-12-wide is-6-wide-sm m-y-20">
			<h5>Or Just try one of these!</h5>
			<p>Phone: (631) 873 - 6058</p>
			<p>E-Mail: frankie.giustino@gmail.com</p>
		</div>
	</div><!-- Grid -->
</div>

@endsection
