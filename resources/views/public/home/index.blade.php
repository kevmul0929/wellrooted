@extends('public.layout.master')

@section('content')
<section class="Hero Hero--Extra_Large Hero__background Background--Primary is-page">
    <div class="Container">
        <div class="Hero__body Content p-y-20 flex-full">
            <div class="Alert Background--Transparent_Black Border-5-Tertiary">
                <div class="Alert__content">
                    <h1>Seek the <br> Balance of <br> Mind and  <br> Body</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="m-y-100">
    <div class="Container">
        <div class="Grid is-spaced">
            @foreach($panels as $panel)
                @include('public.home.partials.panel')
            @endforeach
        </div>
    </div>
</section>

{{-- <section class="m-y-100">
    <div class="Container">
        @foreach($pages as $block)
                @include('public.home.partials.block')
        @endforeach
    </div>
</section> --}}

<!-- <home-block url="home"></home-block>	 -->

@endsection
