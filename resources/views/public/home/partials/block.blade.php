<article class="Column is-12-wide m-t-75">
	<div class="Lego">
            <div class="Card">
                <div class="Hero">
                    <img src="{{$block->image}}" alt="{{$block->title}}">
                </div>
                <div class="Card__body">
					<h1>{{$block->title}}</h1>
					<p>{{nl2br($block->excerpt)}}</p>
					<a href="/pages/{{$block->slug}}" class="Button Button--Secondary">{{$block->button_text}}</a>
				</div><!-- Card -->
				<div class="Card__backer"></div>
			</div><!-- Card__container -->

	</div>
</article>
