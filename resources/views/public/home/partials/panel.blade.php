<div class="Panel Column is-12-wide is-4-wide-sm">
        <div class="Card__body text-is-centered p-b-0">
            <h5>{{$panel->title}}</h5>
        </div>
        <div class="Card__image">
            <div class="svg-fill-primary">
                <?php echo file_get_contents(public_path() . "/img/icons/{$panel->image}");?>
            </div>
            {{-- <img src="/img/icons/{{$panel->image}}" alt=""> --}}
        </div>
        <div class="Card__body text-is-centered p-t-0">{{$panel->description}}</div>
</div>
