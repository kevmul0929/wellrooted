@extends('public.layout.master')

@section('content')
    <div class="Container is-page p-t-20">
        <h1>Let us know how we were!</h1>
        <hr>
        <form action="/testimonial/" method="POST" enctype="multipart/form-data" class="Form p-b-40">
            {{csrf_field()}}
            <div class="Grid is-spaced flex-full">

                <div class="Form__group Column is-12-wide">
                    <label for="profile_image" class="Form__label">A Photo of You</label>
                    <input type="file" id="profile_image" name="profile_image" class="Form__input">
                </div>

                <div class="Form__group Column is-12-wide">
                    <label for="title" class="Form__label">Title</label>
                    <input type="text" id="title" name="title" class="Form__input">
                </div>

                <div class="Form__group Column is-12-wide is-6-wide-sm">
                    <label for="name" class="Form__label">Name</label>
                    <input type="text" id="name" name="name" class="Form__input">
                     @if ($errors->has('name'))
                        <span class="help-block">
                            {{ $errors->first('name') }}
                        </span>
                    @endif
                </div>
                <div class="Form__group Column is-12-wide is-6-wide-sm">
                    <label for="email" class="Form__label">Email</label>
                    <input type="email" id="email" name="email" class="Form__input">
                     @if ($errors->has('email'))
                        <span class="help-block">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>

                <div class="Form__group Column is-12-wide">
                    <textarea name="body" class="Form__input" rows="10" placeholder="How was your experiance?"></textarea>
                     @if ($errors->has('body'))
                        <span class="help-block">
                            {{ $errors->first('body') }}
                        </span>
                    @endif
                </div>

                <div class="Column is-12-wide">
                    <button class="Button Button--Primary" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('footer.scripts')
@endsection
