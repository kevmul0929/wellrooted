@extends('public.layout.master')

@section('content')
    <div class="Container is-page p-y-50">
        <div class="Card">
            @if($testimonial->profile_image)
            <div class="Hero">
                <div class="Hero__body Content text-is-centered">
                    <h1 class="text-is-white">{{ $testimonial->title }}</h1>
                    <div class="divider"></div>
                    <h4 class="text-is-white">{{ $testimonial->name }}</h4>
                </div>
                <img src="{{$testimonial->size('wide')}}" alt="">
            </div>
            @else
            <div class="Card__body p-t-50">
                <h1 class="text-is-centered">{{ $testimonial->title }}</h1>
                <div class="divider"></div>
                <h4 class="text-is-centered">{{ $testimonial->name }}</h4>
            </div>
            @endif
            <div class="Card__body">
                <p class="p-x-50">{!! nl2br($testimonial->body) !!}</p>
            </div>
        </div>
    </div>
@endsection
