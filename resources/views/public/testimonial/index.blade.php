@extends('public.layout.master')

@section('content')
    <div class="Container is-page p-t-20">
        <h1>Testimonials</h1>
        <hr>
        <div class="Grid is-spaced p-y-20">
            @foreach($testimonials as $testimonial)
                <div class="Card Column is-12-wide is-6-wide-sm is-4-wide-md">
                    <a href="{{ $testimonial->link() }}">
                        @if($testimonial->profile_image)
                        <div class="Card__image">
                                <img src="{{ $testimonial->size('medium') }}" alt="">
                        </div>
                        @endif
                    </a>
                    <div class="Card__body is-flex flex-full flex-column">
                        <div class="Card__title">
                            <h6 class="m-b-5 text-is-centered">{{ $testimonial->title }}</h6>
                            <div class="divider"></div>
                            <h6><small>{{ $testimonial->name }}</small></h6>
                        </div>
                        <div class="is-flex flex-full pos-r">
                            <p>{!! $testimonial->excerpt !!}</p>
                        </div>
                        {{-- <a
                            href="/testimonial/{{ $testimonial->id }}"
                            class="Button Button--Primary align-self-flex-end"
                        >
                            Read more...
                        </a> --}}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
