@extends('public.layout.master')

@section('content')

<section class="Container is-page">
	<h1 class="is-huge">404!</h1>
	<p>Well, this is embarrasing! We cannot find the page you requested. Please <a href="/">go back</a> and try again.</p>
</section>

@endsection