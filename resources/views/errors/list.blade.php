@if(count($errors) > 0)
	<div class="notification">
			@foreach($errors->all() as $error)
				<p>{{ $error }}</p>
			@endforeach
	</div>
@endif
