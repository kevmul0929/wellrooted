@extends('admin.layout.master')

@section('content')

<h1>Create a new Page</h1>
@include('errors.list')
<form action="/admin/pages" method="POST" enctype="multipart/form-data">
	@include('admin.pages.partials.form')
</form>

@endsection
