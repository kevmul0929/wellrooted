{{ csrf_field() }}
<div class="control">
	<label for="title" class="label">Title</label>

	<input type="text" name="title" class="input" value="{{ $page->title ?? old('title') }}">

</div>

<div class="control">
	<label for="body" class="label">Body</label>		
	
	<textarea 
		class="textarea" 
		name="body" 
		cols="30" rows="10" 
	>{{ $page->body ?? old('body') }}</textarea>
</div>

<div class="control">
	<button class="button is-primary">Submit</button>
</div>