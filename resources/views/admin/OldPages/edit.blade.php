@extends('admin.layout.master')

@section('content')
<h1>Edit page: {{$page->title}}</h1>
@include('errors.list')
<form action="/admin/pages/{{ $page->id }}" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_method" value="PATCH">
	@include('admin.pages.partials.form')
</form>
@endsection