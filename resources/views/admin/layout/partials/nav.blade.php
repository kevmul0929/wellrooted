@inject('mainPages', 'App\Pages\MainPages')
@inject('testimonials', 'App\Testimonial')
<nav class="navbar has-shadow" role="navigation">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                Frankie G Health
            </a>

            <div :class="['navbar-burger', {'is-active': showDropdown}]" @click="toggleDropdown">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div><!-- navbar-brand -->
        <div :class="['navbar-menu', {'is-active': showDropdown}]">
            <div class="navbar-end">
                <a class="navbar-item" href="/admin/blog">Blog</a>
                <a class="navbar-item" href="/admin/pages">Pages</a>
                <a class="navbar-item" href="/admin/testimonials">
                    Testimonials
                    <span class="badge">{{ $testimonials->getUnpublished() }}</span>
                </a>
                @if(Auth::check())
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">{{ Auth::user()->name }}</a>
                    <div class="navbar-dropdown">
                        <a class="navbar-item" href="/admin/logout">Logout</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</nav>
