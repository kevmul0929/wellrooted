<!DOCTYPE html>
<html>
<head>
    <title>Frankie G | Admin</title>
    <link rel="stylesheet" href="{{ mix('/css/admin.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scaleable=yes">
</head>
<body>
<div id="app">
    @include('admin.layout.partials.nav')

    <div class="container spacer-top">
        <section class="section">
            <div class="content">

                @yield('content')

            </div>
        </section>
        <flash
            title="{{ session('flash_message')['title'] }}"
            message="{{ session('flash_message')['body'] }}"
            type="{{ session('flash_message')['level'] }}"
            important="{{ session('flash_message')['important'] }}"
        ></flash>
    </div>
</div>

<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('/js/admin.js') }}"></script>
@yield('footer.scripts')
</body>
</html>
