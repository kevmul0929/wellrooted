@extends('admin.layout.master')

@section('content')
    <div class="control">
        <data-viewer source="/testimonials" title="Testimonials"></data-viewer>
    </div>
    <modal-delete source="/testimonials"></modal-delete>
@endsection
