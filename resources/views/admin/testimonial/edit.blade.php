@extends('admin.layout.master')

@section('content')
    <div class="content">
        <h1>Editing: Testimonial by {{$testimonial->name}}</h1>

        @include('errors.list')
        <form action="/admin/testimonial/{{$testimonial->id}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PATCH">
                @include('admin.testimonial.partials.form', ["buttontext" => "Edit Testimonial"])
        </form>
    </div>
<script>
@if($testimonial->profile_image)

var oldImage = "{{$testimonial->profile_image}}"
@endif
</script>
@endsection
