{{ csrf_field() }}
<div class="columns">
    <div class="column">

        <div class="control">
            <label for="title" class="label">Title</label>
            <input type="text" name="title" class="input" value="{{ $testimonial->title ? $testimonial->title : old('title') }}">
            @if ($errors->has('title'))
                <span class="has-text-danger">
                    {{ $errors->first('title') }}
                </span>
            @endif
        </div>

        <div class="columns">
            <div class="column">
                <div class="control">
                    <label for="name" class="label">Name</label>
                    <input type="text" name="name" class="input" value="{{ $testimonial->name ? $testimonial->name : old('name') }}">
                    @if ($errors->has('name'))
                        <span class="has-text-danger">
                            {{ $errors->first('name') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="column">
                <div class="control">
                    <label for="email" class="label">Email</label>
                    <input type="email" name="email" class="input" value="{{ $testimonial->email ? $testimonial->email : old('email') }}">
                    @if ($errors->has('email'))
                        <span class="has-text-danger">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="control">
            <label for="body" class="label">Body</label>

            <textarea
                class="textarea"
                name="body"
                cols="30" rows="10"
            >{{ $testimonial->body ? $testimonial->body : old('body') }}</textarea>
            @if ($errors->has('body'))
                <span class="has-text-danger">
                    {{ $errors->first('body') }}
                </span>
            @endif
        </div>

        <div class="control">
            <label for="approved">Is this approved?</label><br>
            <label class="radio">
                <input type="radio" name="approved" value="1" {{ $testimonial->approved ? "checked" : ""}}>
                Yes
            </label>
            <label class="radio">
                <input type="radio" name="approved" value="0" {{ $testimonial->approved ? "" : "checked"}}>
                No
            </label>
        </div>


    </div><!-- FirstColumn -->
    <div class="column">

        <!-- Image -->
        <div class="control">
            <label for="image" class="label">image</label>
            <input type="file" name="image" @change="onFileChange" v-show="!image">
        </div>
        <div>
            <div class="control clear-fix" v-if="image">
                <div class="image box m-t-20">
                    <img :src="image" alt="">
                </div>
                <button class="button is-danger" @click.prevent="removeImage">Remove image</button>
            </div>
        </div>

    </div>
</div>
<div class="columns">
    <div class="comlum">
        <div class="control m-t-20">
            <button class="button is-primary">{{ $buttontext }}</button>
        </div>
    </div>
</div>
