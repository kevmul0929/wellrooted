@extends('admin.layout.master')

@section('content')
    <h1>Create a new Testimonial</h1>
    <form action="/admin/testimonial" method="POST" enctype="multipart/form-data">
        @include('admin.testimonial.partials.form', ["buttontext" => "Create Page"])
    </form>
@endsection
