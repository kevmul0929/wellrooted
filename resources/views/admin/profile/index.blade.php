@extends('admin.layout.master')

@section('content')

<h1>Welcome {{$user->name}}</h1>

<label class="label">Name</label>
<p class="control">
  <input class="input" name="name" type="text">
</p>

<label class="label">E-mail</label>
<p class="control">
  <input class="input" name="email" type="text">
</p>

<label class="label">Password</label>
<p class="control">
  <input class="input" name="password" type="text">
</p>

<label class="label">Confirm Password</label>
<p class="control">
  <input class="input" name="password" type="text">
</p>

<div class="Form__group">
	<button type="submit" class="button is-primary">Edit</button>
</div>

@endsection