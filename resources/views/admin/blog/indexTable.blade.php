@extends('admin.layout.master')

@section('content')
	<div class="control">
		<data-viewer source="/blog" title="Blog Data"></data-viewer>
	</div>
	<modal-delete source="/blog"></modal-delete>
@endsection

@section('footer.scripts')
<script src="/js/admin.js"></script>
@endsection