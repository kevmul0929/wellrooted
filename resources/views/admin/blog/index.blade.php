@extends('admin.layout.master')

@section('content')
	<p class="control">
		<h1 class="title">Blog</h1>
		<a class="button is-primary" href="/admin/blog/create">Create new Blog</a>
	</p>

	<!-- <button @click="orderBy">Order by Name</button> -->

	<div class="columns is-multiline">

		<div v-for="artical in articals">
			<wr-blog>
			</wr-blog>
		</div><!-- column -->
	</div><!-- columns -->

	<div class="modal" :class="{ 'is-active' : showModal }">
		<div class="modal-background"></div>
		<div class="modal-content">
			<div class="card is-fullwidth">
				<header class="card-header">
					<h1 class="card-header-title">
						Are you sure you want to delete?
					</h1>
				</header>
				<div class="card-content">
					<div class="content">
						Once you delete a post, it is deleted forever. You cannot undo this request.
					</div>
				</div>
				<footer class="card-footer">
					<a class="card-footer-item" @click="showModal = false">Never mind</a>
					<a class="card-footer-item" @click="onDelete">KILL IT!</a>
				</footer>
			</div>
		</div>
		<button class="modal-close" @click="showModal = false"></button>
	</div>

@endsection

@section('footer.scripts')

<script src="{{ mix('/js/admin.js') }}"></script>
<script>
	var articals = {!! $articals !!}

	var eventHub = new Vue();

	new Vue({
		el: "#app",

		data: {
			articals: '',
			id: '',
			showModal: false,
			reverse: true
		},

		methods: {
			onDelete: function() {
				axios.delete('/admin/blog/' + this.id)
					.then(response => {
						var id = this.id;
						this.articals = this.articals.filter(function(artical) {
							return artical.id !== id;
						});
						this.id = '';
						this.showModal = false;
						console.log(response.data);
					})
					.catch(error => {
						alert("Sorry, there was an " + error);
					})
			},

			activateModal: function(id) {
				this.id = id;
				this.showModal = true;
			},

			orderBy: function() {
				this.reverse = !this.reverse;
				this.articals = this.articals.sort(function(a, b) {
					var nameA = a.title.toUpperCase();
					var nameB = b.title.toUpperCase();

					if(nameA < nameB) {
						return -1;
					}
					if(nameA > nameB) {
						return 1;
					}
					return 0;
				})
			}
		},

		mounted: function() {
			this.articals = articals;
		},

		created: function() {
			eventHub.$on('activateModal', this.activateModal)
		},

		beforeDestroy: function() {
			eventHub.$off('activateModal', this.activateModal)
		}
	})
</script>

@endsection
