{{ csrf_field() }}
<div class="control">
	<label for="title" class="label">Title</label>

	<input 
		name="title" 
		type="text" 
		class="input" 
		value="{{ $artical->title ? $artical->title : old('title') }}"
	>

</div>

<div class="control">
	<label for="excerpt" class="label">Excerpt</label>		
	
	<textarea 
		name="excerpt" 
		class="textarea" 
		cols="30" rows="10" 
	>{{ $artical->excerpt ? $artical->excerpt : old('excerpt') }}</textarea>
</div>

<div class="control">
	<label for="body" class="label">Body</label>		
	
	<textarea 
		name="body" 
		class="textarea" 
		cols="30" rows="10" 
	>{{ $artical->body ? $artical->body : old('body') }}</textarea>
</div>

<!-- Image -->
<div class="control" v-show="!image">
	<label for="image" class="label">image</label>		
	<input type="file" name="image" @change="onFileChange">
</div>

<div class="control clear-fix" v-show="image">
	<div class="image is-128x128">
		<img :src="image" alt="">
	</div>
	<button class="button is-danger" @click.prevent="removeImage">Remove image</button>
</div>

<!-- Radio -->
<div class="control">
	<label for="approved">Is this approved?</label>
	<label class="radio">
		<input type="radio" name="approved" value="1" {{$artical->approved ? "checked" : ""}}>
		Yes
	</label>
	<label class="radio">
		<input type="radio" name="approved" value="0" {{$artical->approved ? "" : "checked"}}>
		No
	</label>

</div>

<div class="control">
	<button class="button is-primary">Submit</button>
</div>