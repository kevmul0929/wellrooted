{{ csrf_field() }}
<div class="control">
	<label for="title" class="label">Title</label>

	<input 
		type="text" 
		name="title" 
		class="input" 
		v-model="form.title" 
	>

	<span 
		class="help is-danger" 
		v-text="form.errors.get('title')"
		v-if="form.errors.has('title')"
	></span>
</div>

<div class="control">
	<label for="body" class="label">Body</label>		
	
	<textarea 
		class="textarea" 
		name="body" 
		cols="30" rows="10" 
		v-model="form.body"
	></textarea>

	<span 
		class="help is-danger" 
		v-text="form.errors.get('body')"
		v-if="form.errors.has('body')"
	></span>
</div>

<div class="control" v-if="!form.image">
	<label for="image" class="label">image</label>		

	<input
		type="file" 
		name="image" 
		@change="onFileChange"
	>


	<span 
		class="help is-danger" 
		v-text="form.errors.get('image')"
		v-if="form.errors.has('image')"
	></span>
</div>
<div class="control" v-else>
	<div class="image is-64x64">
		<img :src="form.image" alt="">
		<button class="button is-danger" @click="removeImage">Remove image</button>
	</div>
	
</div>

<div class="control">
	<button class="button is-primary" :disabled="form.errors.any()">Submit</button>
</div>