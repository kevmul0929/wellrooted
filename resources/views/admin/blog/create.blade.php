@extends('admin.layout.master')

@section('content')
<div class="columns">
	<div class="column">
		<h1>Create a new Blog</h1>
		@include('errors.list')
		<form action="/admin/blog" method="POST" enctype="multipart/form-data">
			@include('admin.blog.partials.form')
		</form>
	</div>

</div>


@endsection

@section('footer.scripts')

<script>

new Vue({ 

	el: '#app',

	data: {
		image: '',
	},

	methods: {

		onFileChange(e) {
			var files = e.target.files || e.dataTransfer.files;
			if(!files.length)
				return;
			this.createImage(files[0]);
		},

		createImage: function(file){
			var image = new Image();
			var reader = new FileReader();
			var vm = this;

			reader.onload = e => {
				vm.image = e.target.result;
			};
			reader.readAsDataURL(file);	
		},

		removeImage: function(e) {
			this.image = '';
		},
	},

})

</script>
@stop
