@extends('admin.layout.master')

@section('content')
<div class="columns">
	<div class="column">
		<h1 class="title">Editing: {{ $artical->title }}</h1>
		<small>created at {{ $artical->created_at }}</small><br>
		<small>updated at {{ $artical->updated_at }}</small>
		@include('errors.list')
		<form method="POST" action="/admin/blog/{{ $artical->id }}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="PATCH">
			@include('admin.blog.partials.form')
		</form>
	</div>


</div>

@endsection

@section('footer.scripts')

<script>
var oldImage = "{{$artical->size('small')}}"
new Vue({ 


	el : '#app',

	data: {
		image: '',
	},

	methods: {
		onFileChange(e) {
			let files = e.target.files || e.dataTransfer.files;
			if(!files.length)
				return;
			this.createImage(files[0]);
		},

		createImage(file){
			let image = new Image();
			let reader = new FileReader();
			let vm = this;

			reader.onload = e => {
				vm.image = e.target.result;
			};
			reader.readAsDataURL(file);	
		},

		removeImage: function(e) {
			this.image = ''
		},
	},

	computed: {
		computedBody: function() {
			// return this.body.replace('\n', '</p><p>');
			var newBod = this.body.split('\n')
			return newBod.join('</p><p>')
		}
	},

	mounted: function() {
		this.image = oldImage
	}
})
</script>

@stop