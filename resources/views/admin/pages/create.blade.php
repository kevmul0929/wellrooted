@extends('admin.layout.master')

@section('content')

<h1>Create a new Main Page</h1>
@include('errors.list')
<form action="/admin/pages" method="POST" enctype="multipart/form-data">
	@include('admin.pages.partials.form', ["buttontext" => "Create Page"])
</form>

@endsection
