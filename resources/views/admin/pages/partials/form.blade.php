{{ csrf_field() }}
<div class="columns">
    <div class="column">

        <div class="control px-2">
            <label for="title" class="label">Title</label>

            <input type="text" name="title" class="input" value="{{ $page->title ? $page->title : old('title') }}">

        </div>

        <div class="control px-2">
            <label for="excerpt" class="label">Excerpt</label>
            <textarea
                class="textarea"
                name="excerpt"
                cols="30" rows="10"
            >{{ $page->excerpt ? $page->excerpt : old('excerpt') }}</textarea>
        </div>

        <div class="control px-2">
            <label for="body" class="label">Body</label>

            <textarea
                class="textarea"
                name="body"
                cols="30" rows="10"
            >{{ $page->body ? $page->body : old('body') }}</textarea>
        </div>
        <!-- Radio -->
        <div class="control px-2">
            <label for="approved">Is this approved?</label><br>
            <label class="radio">
                <input type="radio" name="approved" value="1" {{ $page->approved ? "checked" : ""}}>
                Yes
            </label>
            <label class="radio">
                <input type="radio" name="approved" value="0" {{ $page->approved ? "" : "checked"}}>
                No
            </label>
        </div>

    </div><!-- FirstColumn -->


    <div class="column">
        <div class="field">
            <label class="label" for="order_by">Placement</label>
            <p class="control px-2">
                <span class="select">
                    <select name="order_by">
                        @for($i; $i >= 1; $i--)
                            <option value="{{$i}}" selected="{{$page->order_by == $i ? true : false}}">{{$i}}</option>
                        @endfor
                    </select>
                </span>
            </p>
        </div>
        <div class="control px-2">
            <label for="button_text" class="label">Button Text</label>

            <input type="text" name="button_text" class="input" value="{{ $page->button_text ? $page->button_text : old('button_text') }}">
        </div>


        <!-- Image -->
        <div class="control px-2">
            <label for="image" class="label">image</label>
            <input type="file" name="image" @change="onFileChange" v-show="!image">
        </div>
        <div>
            <div class="control px-2 clear-fix" v-if="image">
                <div class="image box m-t-20">
                    <img :src="image" alt="">
                </div>
                <button class="button is-danger" @click.prevent="removeImage">Remove image</button>
            </div>
        </div>

    </div>
</div>
<div class="columns">
    <div class="column">
        <div class="control px-2">
            <button class="button is-primary">{{ $buttontext }}</button>
        </div>
    </div>
</div>
