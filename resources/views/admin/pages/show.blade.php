@extends('admin.layout.master')

@section('content')

<h1>{{$page->title}}</h1>
<small>{{$page->slug}}</small>
<a href="/admin/main/{{$page->slug}}/edit" class="button is-primary">Edit Page</a>
<h3>Excerpt</h3>
<p>{{$page->excerpt}}</p>
<h3>Body</h3>
<p>{{$page->body}}</p>
<h3>Button Text</h3>
<button class="button is-info">{{$page->button_text}}</button>
<h3>Image</h3>
<div class="image is-128x128">
	<img src="{{$page->image}}" alt="">
</div>
<h3>Icon</h3>
<div class="image is-64x64">
	<img src="{{$page->icon}}" alt="">
</div>

@endsection