@extends('admin.layout.master')

@section('content')
	<div class="control">
		<data-viewer source="/pages" title="Pages" ></data-viewer>
	</div>
	<modal-delete source="/pages"></modal-delete>
@endsection


@section('footer.scripts')
<script src="/js/admin.js"></script>
@stop