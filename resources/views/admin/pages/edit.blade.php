@extends('admin.layout.master')

@section('content')
<h1>Editing: {{$page->title}}</h1>

@include('errors.list')
<form action="/admin/pages/{{$page->slug}}" method="POST" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PATCH">
        @include('admin.pages.partials.form', ["buttontext" => "Edit Page"])
</form>
<script>
@if($page->image)
var oldImage = "{{$page->size('medium')}}"
@endif
</script>
@endsection
