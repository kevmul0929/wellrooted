@extends('admin.layout.master')

@section('content')
    <div class="Container">
        <div class="column is-half is-offset-one-quarter">

            <h3>Login</h3>
            <form role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <!-- email -->
                <div class="field">
                    <label class="label">Email</label>
                    <div class="control">
                        <input class="input" type="text" name="email">
                    </div>
                </div>
                <!-- password -->
                <div class="field">
                    <label class="label">Password</label>
                    <div class="control">
                        <input class="input" type="password" name="password">
                    </div>
                </div>

                <!-- Remember -->
                <div class="field">
                    <div class="control">
                        <label class="checkbox">
                            <input type="checkbox" name="remember"> Remember me
                        </label>
                    </div>
                </div>

                <!-- Buttons -->
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-primary" type="submit">
                            <i class="fa fa-btn fa-sign-in pr-2"></i> Login
                        </button>
                    </div>
                    <div class="control">
                        <button class="button is-text" href="{{ url('/password/reset') }}">Forgot Password</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
