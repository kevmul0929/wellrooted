@extends('admin.layout.master')

@section('content')
    <div class="Container">
        <div class="column is-half is-offset-one-quarter">

            <h3>Register</h3>
            <form role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}

                {{-- name --}}
                <div class="field">
                    <label for="name" class="label">Name</label>
                    <input autocomplete="off" type="text" id="name" name="name" class="input{{$errors->has('name') ? ' is-danger' : ''}}" value="{{old('name')}}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong class="has-text-danger is-size-7">{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <!-- email -->
                <div class="field">
                    <label class="label">Email</label>
                    <div class="control">
                        <input autocomplete="off" class="input{{$errors->has('email') ? ' is-danger' : ''}}" value="{{old('email')}}" type="text" name="email">
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong class="has-text-danger is-size-7">{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <!-- password -->
                <div class="field">
                    <label class="label">Password</label>
                    <div class="control">
                        <input autocomplete="off" class="input{{$errors->has('password') ? ' is-danger' : ''}}" type="password" name="password">
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong class="has-text-danger is-size-7">{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                {{-- confirm password --}}
                <div class="field">
                    <label for="password-confirm" class="label">Confirm Password</label>
                    <input autocomplete="off" type="password" id="password-confirm" name="password_confirmation" class="input{{$errors->has('password') ? ' is-danger' : ''}}">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong class="has-text-danger is-size-7">{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <!-- Remember -->
                <div class="field">
                    <div class="control">
                        <label class="checkbox">
                            <input autocomplete="off" type="checkbox" name="remember"> Remember me
                        </label>
                    </div>
                </div>

                <!-- Buttons -->
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-primary" type="submit">
                            <i class="fa fa-btn fa-sign-in pr-2"></i> Register
                        </button>
                    </div>
                    <div class="control">
                        <button class="button is-text" href="{{ url('/password/reset') }}">Forgot Password</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
