import './bootstrap';

import Flash from './components/Flash.vue';

new Vue({
	el: '#app',

	data: {
        displayNav: false,
        rating: 0,
		dropDown: false,
		dropList: {
			id : "",
			active: true
		}
	},

    components: {
        Flash
    },

	methods: {
		toggleDropDown() {
			this.dropDown = !this.dropDown
		},

		toggleList(event) {
			alert(event.target)
		},

        toggleNav() {
            this.displayNav = !this.displayNav;
        }
	},

	computed: {
		showDropDown() {
			return this.dropDown
		}
	},

	created() {
	}
})
