import './bootstrap'

import DataViewer from './components/DataViewer.vue'
import ModalDelete from './components/ModalDelete.vue'
import Flash from './components/Flash--Bulma.vue'

window.Event = new Vue();

new Vue({
    el: '#app',

    props: ['oldImage'],

    components: {
        DataViewer,
        ModalDelete,
        Flash
    },

    data: {
        showDropdown: false,
        image: '',
    },

    methods: {
        toggleDropdown(){
            this.showDropdown = !this.showDropdown;
        },

        onFileChange(e) {
            let files = e.target.files || e.dataTransfer.files;
            if(!files.length)
                return;
            this.createImage(files[0]);
        },

        createImage(file){
            let image = new Image();
            let reader = new FileReader();
            let vm = this;

            reader.onload = e => {
                vm.image = e.target.result;
            };
            reader.readAsDataURL(file);
        },

        removeImage: function(e) {
            this.image = '';
        }
    },

    mounted() {
        if(oldImage)
            this.image = oldImage
    }


})
