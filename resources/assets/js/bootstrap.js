import Vue from 'vue';
import axios from 'axios';
import _ from 'lodash';
//import Form from './classes/Form.js';

window.Vue = Vue;
window.axios = axios;
//window.Form = Form;
window._ = _;
window.Events = new Vue();

window.flash = function (title ,message = null, type='success', toggle=false) {
    window.Events.$emit('flash', title, message, type, toggle);
}; // flash('my new flash message')

window.axios.defaults.headers.common = {
	'X-Requested-With': 'XMLHttpRequest'
};
