(function() {

	"use strict";

	var menu		= document.getElementById('CN__nav'),
		toggle		= document.getElementById('CN__nav__toggle'),
		mask		= document.createElement("div"),
		activeClass	= "is-active";

	/**
	 * Create Mask
	 */
	mask.classList.add("CN__nav__mask");
	document.body.appendChild(mask);

	/**
	 * Listen for clicks on the toggle
	 */
	toggle.addEventListener("click", function(e){
		e.preventDefault();
		toggle.classList.contains(activeClass) ? deactivateMenu() : activateMenu();
	});

	/**
	 * Listen for clicks on the mask, which should close the menu
	 */
	mask.addEventListener("click", function(){
		deactivateMenu();
		console.log('click');
	});

	function activateMenu() {
		menu.classList.add(activeClass);
		toggle.classList.add(activeClass);
		mask.classList.add(activeClass);
	}

	function deactivateMenu(){
		menu.classList.remove(activeClass);
		toggle.classList.remove(activeClass);
		mask.classList.remove(activeClass);
	}
})();