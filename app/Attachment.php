<?php

namespace App;

use Image;
use App\File;

class Attachment extends File
{
    private $extension = '';
    private $fileName = '';
    private $image_dir = '/storage/img/';

    public static $thumbnailSizes = [
        'large' => [
            'height'    => null,
            'width'     => 600],
        'wide' => [
            'height'    => 300,
            'width'     => 1000],
        'medium' => [
            'height'    => null,
            'width'     => 300],
        'small' => [
            'height'    => 64,
            'width'     => 64],
    ];


    public function handle()
    {
        $this->setup()
             ->sanitizeName($this->file)
             ->makeMultiResolutionImages($this->file);
        $this->image = $this->directory . $this->fileName . $this->extension;
    }

    public function setDirectory()
    {
        $this->directory = $this->image_dir . $this->yearMonth . '/';
    }

    /**
     * Create the name of Featured image
     *
     * @param  UploadedFile $file
     * @return String Name
     */
    private function sanitizeName($file)
    {
        $this->extension = '.' . $file->getClientOriginalExtension();
        $basename = str_replace($this->extension, '', $file->getClientOriginalName());
        $this->fileName = str_replace(' ', '_', $this->time . '__' . $basename);

        return $this;
    }


    /**
     * Take the uploaded image and save as several sizes
     *
     * @param  File $file
     */
    public function makeMultiResolutionImages($file)
    {
        $img = Image::make($file->getRealPath())
              ->save($this->getDir());

        $img->backup();

        foreach(static::$thumbnailSizes as $thumbnail => $value)
        {
            $img->reset();
            if($value['width'] == $value['height'])
            {
                $img->fit($value['width'])->save($this->getDir($thumbnail));
                continue;
            }

            else if(($value['width'] && $value['height']) && ($value['width'] != $value['height']))
            {
                $img->resizeCanvas($value['width'], $value['height'])->save($this->getDir($thumbnail));
                continue;
            }
            else {
                $img->fit($value['width'], $value['height'], function ($constraint){
                    $constraint->aspectRatio();
                })->save($this->getDir($thumbnail));
            }
        }

        $img->destroy();
    }


    public function getDir($thumbnail = null)
    {
        if($thumbnail)
        {
            $thumb = '--' . $thumbnail;
        }else{
            $thumb = '';
        }
        return public_path() . $this->directory . $this->fileName . $thumb . $this->extension;
    }


    public static function destroy($source)
    {
        if(file_exists(public_path() . $source))
        {
            $image = explode('.', $source);
            foreach(self::$thumbnailSizes as $key => $value){
                $i = $image[0] . '--' . $key . '.' . $image[1];
                if(file_exists(public_path() . $i))
                {
                    unlink(public_path() . $i);
                }
            }

            unlink(public_path() . $source);
        }
    }


}
