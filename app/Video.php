<?php 

namespace App;

use App\File;
use Image;
// use Thumbnail;

class Video extends File
{
	protected $video_dir = '/storage/video/';


	public function handle()
	{
		$this->setup()
			 ->sanitizeName($this->file)
			 ->place()
			 ->createImage();
	}

	/**
	 * Set up the Directory for persisting the file
	 * 
	 */
	public function setDirectory()
	{
		$this->directory = $this->video_dir . $this->yearMonth . '/';
		return $this;
	}

	private function sanitizeName($file)
	{
		$this->extension = '.' . $file->getClientOriginalExtension();
		$basename = str_replace($this->extension, '', $file->getClientOriginalName());
    	$this->fileName = str_replace(' ', '_', $this->time . '__' . $basename);

    	return $this;
	}

	public function place()
	{
		$this->file->move(public_path() . $this->directory, $this->fileName . $this->extension);
		return $this;
	}


	private function createImage() 
	{
		// $img = Thumbnail::getThumbnail($this->file, $this->directory, $this->fileName . '.png', 64, 64, 25, '', true);

		dd($this->file);

		$img = Image::make($this->file[0]);

		dd($img);
		return $this;
	}


}