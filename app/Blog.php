<?php

namespace App;

Use App\Attachment;
use App\Helper\DataViewer;
use App\Uploader;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    use DataViewer;

    protected $casts = [
        'approved' => 'boolean',
    ];

    protected $table = 'blog';

    protected $fillable = [
        'title',
        'body',
        'image',
        'approved'
    ];

    public static $columns =  [
        'image', 'id', 'title', 'approved',
    ];



    public static function persist($data, $file)
    {
        $artical = new static;
        $artical->image = $file;
        $artical->title = $data['title'];
        $artical->excerpt = $data['excerpt'];
        $artical->body = $data['body'];
        $artical->approved = $data['approved'];
        $artical->createSlug();

        return $artical;
    }


    /**
     * Create a slug from the title of the post and add a numeric number greater than previous post by 1
     *
     * @param  String $title
     * @return String
     */
    public function createSlug()
    {
        $this->slug = str_slug($this->title);
        $latestSlug =
            static::whereRaw("slug RLIKE '^{$this->slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug');

        if($latestSlug->toArray())
        {
            $pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $this->slug .= '-' . ($number + 1);
        }
    }


    /**
     * Get a specified size, if it exists, and return it
     *
     * @param  String $size
     * @return String
     */
    public function size($size)
    {
       if(!$this->image)
       {
            return;
       }
       if(!array_key_exists($size, Attachment::$thumbnailSizes))
       {
            return $this->image;
       }
       $image = explode('.', $this->image);
       $newImage = $image[0] . '--' . $size . '.' . $image[1]; ;
       if(!file_exists(public_path() . $newImage))
       {
            return $this->image;
       }
       return $newImage;
    }



    /**
     * Destroy images for a Blog
     *
     * @return this
     */
    public function destroyImages()
    {
        if($this->image)
        {
            Attachment::destroy($this->image);
        }

        return $this;
    }


    /**
     * Toggle the approval of a Blog post
     *
     * @return Boolean
     */
    public function toggleApproved()
    {
        if($this->approved)
        {
            return $this->approved = 0;
        }
        return $this->approved = 1;
    }


    /**
     * Upadate image if a new image has been added
     *
     * @param  [type] $image [description]
     * @return [type]        [description]
     */
    public function updateImage($image)
    {
        if(!$image) return;

        if($this->image)
        {
            $this->destroyImages();
        }

        return $this->image = Uploader::file($image);
    }

}
