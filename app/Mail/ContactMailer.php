<?php 

namespace App\Mail;

use App\Mail\Mailer;

class ContactMailer extends Mailer
{
	public function send($request)
	{
		$view = 'emails.contact';
		$subject = 'Site contact form';
		$recipient = 'frankie.giustino@gmail.com';

		return $this->sendTo($recipient, $subject, $view, $request->all());
	}
}