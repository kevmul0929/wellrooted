<?php 

namespace App\Mail;

use Mail;

abstract class Mailer 
{
	public function sendTo($recipient, $subject, $view, $data= [])
	{
		Mail::send($view, $data, function($message) use($recipient, $subject, $data) {
			$message->to($recipient)
					->cc(self::addCc($data))
					->subject($subject);
		});
	}

	public function addCc($data)
	{
		if(isset($data['cc']))
		{
			return $data['email'];
		}
		return [];
	}
}