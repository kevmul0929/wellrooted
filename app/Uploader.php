<?php 

namespace App;

use Input;

class Uploader
{
	public static $lookup = [
		'image' => Attachment::class,
		'video' => Video::class
	];
	
	public static function file($upload)
	{
		if($upload !== null)
		{		
			$type = explode('/', $upload->getMimeType());

			$file = new static::$lookup[$type[0]]($upload);

			$file->handle();

			return $file->image;
		}
	}
}