<?php

namespace App;

use App\Attachment;
use App\Helper\DataViewer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use DataViewer;

    protected $fillable = ['name', 'title', 'email', 'body'];

    protected $casts = [
        'approved' => 'boolean',
    ];

    public static $columns = [
        'id',
        'name',
        'email',
        'approved'
    ];


    /**
     * Publish a Testimonial now or given time
     *
     * @param  Carbon/Carbon $dateTime
     * @return self
     */
    public function publish()
    {
        $this->approved = true;
        $this->save();

        return $this;
    }

    public function size($size)
    {
        if(!array_key_exists($size, Attachment::$thumbnailSizes))
        {
            return $this->profile_image;
        }
        $image = explode('.', $this->profile_image);
        $newImage = $image[0] . '--' . $size . '.' . $image[1]; ;
        if(!file_exists(public_path() . $newImage))
        {
            return $this->image;
        }
        return $newImage;
    }


    public function toggleApproved()
    {
        if($this->approved)
        {
            return $this->approved = 0;
        }
        return $this->approved = 1;
    }

    public function getUnpublished()
    {
        return self::where('approved', '=', '0')
            ->where('updated_at', '>=', Carbon::parse('-1 week') )
            ->count();
    }

    public function link()
    {
        return "/testimonial/{$this->id}";
    }

    /*|--------------------------------------------------------
      | Query Scopes
      |------------------------------------------------------*/

    /**
     * Return only Published Testimonials
     *
     * @param  Query $query
     * @return Query query
     */
    public function scopePublished($query)
    {
        return $query->where('approved', '=', 1)->orderBy('updated_at', 'desc');
    }

    /*|--------------------------------------------------------
      | Attributes
      |------------------------------------------------------*/

    public function getExcerptAttribute()
    {
        $text = $this->body;
        if(str_word_count($text, 0) > 50) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[50]) . "...<a style=\"bottom: 0;right: 0;\" class=\"Button Button--Primary pos-a\" href=\"/testimonial/{$this->id}\">Read More</a>";
        }
        return $text;
    }
}
