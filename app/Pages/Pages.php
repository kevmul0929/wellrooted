<?php

namespace App\Pages;

use Carbon\Carbon;
use App\Helper\DataViewer;
use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
	use DataViewer;
    
    protected $fillable = ['title', 'body'];

    public static $columns = ['id', 'title', 'updated_at', 'created_at', 'approved'];

    public static function persist($data)
    {
    	$page = new self;
    	$page->createSlug($data['title']);
        $page->title = $data['title'];
        $page->body = $data['body'];

    	return $page;
    }

    /**
	 * Create a slug from the title of the post and add a numeric number greater than previous post by 1
	 * 
	 * @param  String $title 
	 * @return String
	 */
	public function createSlug($title) 
	{
		$this->slug = str_slug($title);
		$latestSlug =
            static::whereRaw("slug RLIKE '^{$this->slug}(-[0-9]*)?$'")
                ->latest('id')
                ->pluck('slug');

        if($latestSlug)
        {
            $pieces = explode('-', $latestSlug);
            $number = intval(end($pieces));
            $this->slug .= '-' . ($number + 1);
        }
	}

    /**
     * Toggle the approval of a Blog post
     * 
     * @return Boolean 
     */
    public function toggleApproved()
    {
        if($this->approved)
        {
            return $this->approved = 0;
        }
        return $this->approved = 1;
    }
}
