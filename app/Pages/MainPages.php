<?php

namespace App\Pages;

use App\Attachment;
use App\Helper\DataViewer;
use App\Uploader;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MainPages extends Model
{
    use DataViewer;

    protected $casts = [
        'approved' => 'boolean',
    ];

    protected $fillable = [
        'title',
        'excerpt',
        'body',
        'image',
        'button_text',
        'icon',
        'approved',
        'slug',
        'order_by'
    ];

    public static $columns = [
        'image',
        'id',
        'order_by',
        'title',
        'button_text',
        'approved'
    ];

    public static function persist($data, $file)
    {
        $page = new static;
        $page->image = $file;
        $page->title = $data['title'];
        $page->excerpt = $data['excerpt'];
        $page->body = $data['body'];
        $page->button_text = $data['button_text'];
        $page->createSlug($data['title']);
        $page->order_by = $data['order_by'];
        $page->increaseOrder($page->order_by);
        $page->save();

        return $page;
    }

    /**
	 * Create a slug from the title of the post and add a numeric number greater than previous post by 1
	 *
	 * @param  String $title
	 * @return String
	 */
	public function createSlug($title)
	{
        // $this->slug = hash('sha1', random_bytes(10));
		$this->slug = str_slug($title);

		$latestSlug =
            static::whereRaw("slug LIKE '{$this->slug}%'")
                ->latest('id')
                ->pluck('slug');

        if($latestSlug->count() != 0)
        {
            $this->slug .= '-' . $latestSlug->count();
        }
	}

    /**
     * Toggle the approval of a Blog post
     *
     * @return Boolean
     */
    public function toggleApproved()
    {
        if($this->approved)
        {
            return $this->approved = 0;
        }
        return $this->approved = 1;
    }

    /**
     * Get a specified size, if it exists, and return it
     *
     * @param  String $size
     * @return String
     */
    public function size($size)
    {
        if(!array_key_exists($size, Attachment::$thumbnailSizes))
        {
            return $this->image;
        }
        $image = explode('.', $this->image);
        $newImage = $image[0] . '--' . $size . '.' . $image[1]; ;
        if(!file_exists(public_path() . $newImage))
        {
            return $this->image;
        }
        return $newImage;
    }

    public function updateImage($image)
    {
        if(!$image) return;
        if($this->image)
        {
            $this->destroyImages();
        }

        return $this->image = Uploader::file($image);
    }

    public function destroyImages()
    {
        Attachment::destroy($this->image);

        return $this;
    }


    public function reOrder($newOrder)
    {
        if($this->order_by != $newOrder)
        {
            $this->decreaseOrder();
            $this->increaseOrder($newOrder);
            $this->order_by = $newOrder;
        }
    }

    public function decreaseOrder()
    {
        $pages = self::where('order_by', '>', $this->order_by)->get();
        foreach($pages as $page)
        {
            $page->order_by -= 1;
            $page->update();
        }
        return;
    }

    private function increaseOrder($newOrder)
    {
        $pages = self::where('order_by', '>=', $newOrder)->get();

        foreach($pages as $page)
        {
            $page->order_by += 1;
            $page->update();
        }
    }

    public function getLastOrder()
    {
        return self::select('order_by')
            ->orderBy('order_by', 'desc')
            ->first();
    }
}
