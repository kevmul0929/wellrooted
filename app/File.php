<?php 

namespace App;

use Carbon\Carbon;

class File 
{
	protected $file;

	protected $time;
	protected $yearMonth;

	public $directory = '';

	function __construct($file)
	{
		$this->file = $file;
		$this->time = str_replace(':', '-', Carbon::now());
		$this->yearMonth = strtolower(date('Y/M'));
	}

	/**
	 * Set up all directories
	 * 
	 */
	public function setUp()
	{
		$this->setDirectory();
    	$this->makeDirectory();
    	return $this;
    }


	/**
	 * Create the directories for files to save
	 * 
	 * @return bool 
	 */
	private function makeDirectory()
	{
		if(!is_dir(public_path() . $this->directory))
    	{
    		mkdir(public_path() . $this->directory, 0777, true);
    	}
	}

	/**
	 * Take the uploaded image and save as several sizes
	 * 
	 * @param  File $file 
	 */
	public function makeMultiResolutionImages($file)
	{
		$img = Image::make($file->getRealPath())
    		  ->save($this->getDir());
    		  
    	foreach(static::$thumbnailSizes as $thumbnail => $value)
    	{
    		if($value['width'] == $value['height'])
    		{
    			$img->fit($value['width'])->save($this->getDir($thumbnail));
    			continue;
    		}

    		else if(($value['width'] && $value['height']) && ($value['width'] != $value['height']))
    		{
    			$img->resizeCanvas($value['width'], $value['height'])->save($this->getDir($thumbnail));
    			continue;
    		}
    		else {
	            $img->resize($value['width'], $value['height'], function ($constraint){
	            	$constraint->aspectRatio();
	            })->save($this->getDir($thumbnail));
    		}
	    }

	    $img->destroy();
	}
}