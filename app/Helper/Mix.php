<?php


function mix($source)
{
    $manifest = json_decode(file_get_contents('./mix-manifest.json'));
    foreach($manifest as $key => $value)
    {
        if($key == $source)
        {
            return $value;
        }
    }
}
