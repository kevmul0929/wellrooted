<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Uploader;
use App\Http\Requests;
use App\Blog;

class AdminBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articals = Blog::latest('updated_at')->get();

        return view('admin.blog.indexTable', compact('articals'));
    }

    public function getData()
    {
        $model = Blog::searchPaginateAndOrder();
        $columns = Blog::$columns;

        return response()
            ->json([
                'model' => $model,
                'columns' => $columns
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $artical = new Blog;
        return view('admin.blog.create', compact('artical'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'title'     => 'required|min:3',
            'excerpt'   => 'required|min:5',
            'body'      => 'required|min:5',
            'image'     => 'mimes:jpeg,gif,png'
        ]);
        $file = Uploader::file($request->file('image'));

        $blog = Blog::persist($request->all(), $file);

        $blog->save();

        return redirect('/admin/blog/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artical = Blog::findOrFail($id);

        return view('admin.blog.edit', compact('artical'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'title'     => 'required|min:3',
            'excerpt'   => 'required|min:5',
            'body'      => 'required|min:5',
        ]);

        $blog = Blog::findOrFail($id);

        $blog->updateImage($request->image);

        $blog->update([
            'title'     => $request->title,
            'excerpt'   => $request->excerpt,
            'body'      => $request->body,
            'approved'  => $request->approved,
        ]);


        return redirect('/admin/blog');
    }

    public function toggleApproved(Request $request, $id)
    {
        $artical = Blog::findOrFail($id);

        $artical->toggleApproved();

        $artical->update();

        return response('sucsessfull');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);

        $blog->destroyImages()->delete();

        return ['message' => 'Data Deleted!'];
    }
}
