<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pages\Pages;

class OldPagesController extends Controller
{
	public function index()
	{
		return view('admin.pages.index');
	}

	public function create()
	{
		return view('admin.pages.create');
	}

	public function store(Request $request)
	{
		$this->validate(request(), [
            'title' => 'required|min:3',
            'body'  => 'required|min:5',
        ]);

		$page = Pages::persist($request->all());
		$page->save();

		return redirect('/admin/pages');
	}

	public function edit($id)
	{
		$page = Pages::findOrFail($id);

		return view('admin.pages.edit', compact('page'));
	}

	public function show($slug)
	{
		$page = Pages::where([
				['slug', '=', $slug],
				['approved', '=', 1]
			])->firstOrFail();

		return view('public.pages.show', compact('page'));
	}

	public function update(Request $request, $id)
	{
		$page = Pages::findOrFail($id);

		$page->update($request->all());

		return redirect('/admin/pages');
	}

	public function destroy($id)
	{
		(Pages::findOrFail($id))->delete();
	}

    public function getData()
    {
    	// $model = Pages::columns;
    	$model = Pages::searchPaginateAndOrder();
        $columns = Pages::$columns;

        return response()
            ->json([
                'model' => $model,
                'columns' => $columns
            ]);
    }

    public function toggleApproved(Request $request, $id)
    {
        $page = Pages::findOrFail($id);

        $page->toggleApproved();

        $page->update();

        return response('sucsessfull');
    }

}
