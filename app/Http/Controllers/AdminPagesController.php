<?php

namespace App\Http\Controllers;

use App\Uploader;
use App\Http\Requests;
use App\Pages\MainPages;
use Illuminate\Http\Request;

class AdminPagesController extends Controller
{
    public function index()
    {
        return view('admin.pages.index');
    }

    public function show($slug)
    {
        $page = MainPages::where('slug', $slug)->firstOrFail();

        return view('admin.pages.show', compact('page'));
    }

    public function create()
    {
        $page = new MainPages;

        $i = MainPages::count() + 1;

        return view('admin.pages.create', compact('page', 'i'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate(request(), [
            'title'     => 'required|min:3',
            'excerpt'   => 'required|min:5',
            'body'      => 'required|min:5',
            'image'     => 'required|mimes:jpeg,gif,png'
        ]);

        $file = Uploader::file($request->file('image'));

        $page = MainPages::persist($request->all(), $file);

        return redirect('/admin/pages/');
    }

    public function edit($id)
    {
        $page = MainPages::findOrFail($id);

        $i = MainPages::count();

        return view('admin.pages.edit', compact('page', 'i'));
    }

    public function update(Request $request, $slug)
    {
        $this->validate(request(), [
            'title'     => 'required|min:3',
            'excerpt'   => 'required|min:5',
            'body'      => 'required|min:5',
            'image'     => 'mimes:jpeg,gif,png'
        ]);

        $page = MainPages::where('slug', $slug)->firstOrFail();
        $page->updateImage($request->image);
        $page->reOrder((int)$request->order_by);
        $page->update([
            'title'     => $request->title,
            'excerpt'   => $request->excerpt,
            'body'      => $request->body,
            'button_text'   => $request->button_text,
            'icon'      => $request->icon,
        ]);

        return redirect('/admin/pages/');
    }

    public function destroy($id)
    {
        $page = MainPages::findOrFail($id);
        $page->decreaseOrder();
        $page->delete();
    }

    public function getData()
    {
        $model = MainPages::searchPaginateAndOrder();
        $columns = MainPages::$columns;

        return response()
            ->json([
                'model' => $model,
                'columns' => $columns
            ]);
    }

    public function toggleApproved(Request $request, $id)
    {
        $page = MainPages::findOrFail($id);

        $page->toggleApproved();

        $page->update();

        return response('sucsessfull');
    }


}
