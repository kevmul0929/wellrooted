<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Blog;

class AdminHomeController extends Controller
{
	public function index()
	{
		return redirect('/admin/blog/');
	}
}
