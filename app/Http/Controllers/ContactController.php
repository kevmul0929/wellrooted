<?php

namespace App\Http\Controllers;

use App\Mail\ContactMailer;
use Mailgun\Mailgun;
use App\Http\Requests;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    protected $mailer;

    /**
     * Class Constructor
     * @param    $mailer   
     */
    public function __construct(ContactMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function index()
    {
    	return view('public.contact.index');
    }

    public function deliver(Request $request)
    {
    	$this->validate(request(), [
            'name' => 'required|',
            'email'  => 'required|email',
            'body' => 'required|min:5'
        ]);

        $data = [];

        $this->mailer->send($request);

        return redirect('/contact')->with('Success');
    
    }
}
