<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Pages\MainPages;
use Markdown;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function show($slug)
    {
    	$page = MainPages::where('slug', $slug)
    		->where('approved', 1)
    		->firstOrFail();

        $page->body = Markdown::parse($page->body);

    	return view('public.pages.show', compact('page'));
    }
}
