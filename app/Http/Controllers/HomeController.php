<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Pages\MainPages;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $panels = [
            'panel1' => (object) [
                'title' => 'Regeneration',
                'image' => 'WellRootedIcons__02-03.svg',
                'description' => 'Rejuvenate your body.',
            ],
            'panel2' => (object) [
                'title' => 'Source',
                'image' => 'WellRootedIcons__02-02.svg',
                'description' => 'Get to the root of the problems.',
            ],
            'panel3' => (object) [
                'title' => 'Digestive',
                'image' => 'WellRootedIcons__02-01.svg',
                'description' => 'Get the proper health for your digestive system.',
            ],
            // 'panel4' => (object) [
            //     'title' => 'Intuitive Massage',
            //     'image' => '',
            //     'description' => 'Get you hands'
            // ]
        ];
        return view('public.home.index', compact('panels'));
    }

    public function getData()
    {
    	$blocks = MainPages::where('approved', true)->get();

    	return response($blocks);
    }
}
