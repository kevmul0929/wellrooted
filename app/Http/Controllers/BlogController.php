<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Http\Requests;
use Markdown;
use Illuminate\Http\Request;

class BlogController extends Controller
{
	public function index()
	{
		$blogs = Blog::where('approved', '=', 1)
			->latest()
			->get();

		return view('public.blog.index', compact('blogs'));
	}

	public function show($slug)
	{
		$artical = Blog::where([
				['slug', '=', $slug],
				['approved', '=', 1]
			])->firstOrFail();

		$artical->body = Markdown::parse($artical->body);


		return view('public.blog.show', compact('artical'));
	}
}
