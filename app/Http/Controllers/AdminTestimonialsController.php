<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Testimonial;
use App\Uploader;
use Illuminate\Http\Request;

class AdminTestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.testimonial.index');
    }

    /**
     * Get data for DataViewer
     *
     * @return Array
     */
    public function getData()
    {
        $model = Testimonial::searchPaginateAndOrder();
        $columns = Testimonial::$columns;

        return response()
            ->json([
                'model' => $model,
                'columns' => $columns
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $testimonial = new Testimonial;
        return view('admin.testimonial.create', compact('testimonial'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
            'title' => 'required',
            'email' => 'required|email',
            'body' => 'required',
            'profile_image' => 'image'
        ]);
        $testimonial = new Testimonial($request->all());

        if(request('profile_image')){
            $file = Uploader::file($request->file('profile_image'));
            // $testimonial->profile_image = Storage::disk('public')->()
            $testimonial->profile_image = $file;
        }
        $testimonial->save();

        flash('Success', 'Your testimonial was submitted successfully')->success();
        return redirect('/admin/testimonials');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        return view('admin.testimonial.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testimonial = Testimonial::findOrFail($id);

        if($request->file('image') !== null && $request->file('image') !== $testimonial->profile_image)
        {
            if($request->file('image') != null)
            {
                $file = Uploader::file($request->file('image'));
                $testimonial->profile_image = $file;
            }else{
                Attachment::destroy($testimonial->profile_image);
                $testimonial->profile_image = null;
            }

        }
        $testimonial->update([
            'title'         => request('title'),
            'name'          => request('name'),
            'email'         => request('email'),
            'body'          => request('body'),
            'profile_image' => request('image') ?? $testimonial->profile_image
        ]);

        flash('Success', 'Testimonial updated')->success();
        return redirect('/admin/testimonials');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::findOrFail($id);
        Attachment::destroy($testimonial->profile_image);
        $testimonial->delete();
    }


    public function toggleApproved(Request $request, $id)
    {
        $testimonial = Testimonial::findOrFail($id);

        $testimonial->toggleApproved();

        $testimonial->update();

        return response('sucsessfull');
    }
}
