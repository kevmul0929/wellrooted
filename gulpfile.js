var elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.sass')
       .sass([
      		'./node_modules/bulma/bulma.sass',
      		'./node_modules/font-awesome/css/font-awesome.min.css',
       		'admin/admin.sass'
       	], './public/css/admin.css')
       .webpack('app.js', './public/js/app.js')
       .copy('./node_modules/font-awesome/fonts', 'public/fonts');
});
