<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('api/home', 'HomeController@getData');

Route::get('about', 'AboutController@index');

Route::get('blog', 'BlogController@index');
Route::get('blog/{slug}', 'BlogController@show');

Route::get('massage', 'MassageController@index');

Route::get('pages/{slug}', 'PagesController@show');

Route::get('testimonials', 'TestimonialsController@index');
Route::get('testimonial/create', 'TestimonialsController@create');
Route::get('testimonial/{id}', 'TestimonialsController@show');
Route::post('testimonial', 'TestimonialsController@store');

Route::get('contact', 'ContactController@index');
Route::post('contact', 'ContactController@deliver');


// Admin
Route::group(['middleware' => 'auth'], function() {

    Route::get('admin', 'AdminHomeController@index');

    Route::resource('admin/profile', 'AdminProfileController@index');

    Route::resource('admin/blog', 'AdminBlogController');

    Route::get('api/blog', 'AdminBlogController@getData');
    Route::patch('api/blog/{id}', 'AdminBlogController@toggleApproved');
    Route::delete('api/blog/{id}', 'AdminBlogController@destroy');

    Route::resource('admin/pages', 'AdminPagesController');

    Route::get('api/pages', 'AdminPagesController@getData');
    Route::patch('api/pages/{id}', 'AdminPagesController@toggleApproved');
    Route::delete('api/pages/{id}', 'AdminPagesController@destroy');

    Route::get('admin/testimonials', 'AdminTestimonialsController@index');
    Route::get('admin/testimonials/create', 'AdminTestimonialsController@create');
    Route::get('admin/testimonials/{id}/edit', 'AdminTestimonialsController@edit');
    Route::post('admin/testimonial', 'AdminTestimonialsController@store');
    Route::patch('admin/testimonial/{id}', 'AdminTestimonialsController@update');

    Route::get('api/testimonials', 'AdminTestimonialsController@getData');
    Route::patch('api/testimonials/{id}', 'AdminTestimonialsController@toggleApproved');
    Route::delete('api/testimonials/{id}', 'AdminTestimonialsController@destroy');
});

Route::auth();

Route::get('admin/logout', function(){
    Auth::logout();
    return redirect('/');
});

Route::get('/home', 'HomeController@index');
